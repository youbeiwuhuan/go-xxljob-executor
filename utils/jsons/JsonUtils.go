package jsons

import (
	"encoding/json"
)

func ToJsonString(o interface{}) string {
	data, err := json.Marshal(o)
	if err != nil {
		return ""
	}

	return string(data)
}

func JsonStringToObj(jsonStr string, objPoint interface{}) {
	err := json.Unmarshal([]byte(jsonStr), objPoint)
	if nil != err {
		panic(err)
	}
}
