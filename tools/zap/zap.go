// @Title  请填写文件名称（需要改）
// @Description  请填写文件描述（需要改）
// @Author  请填写自己的真是姓名（需要改）  2021/6/27 0:06
// @Update  请填写自己的真是姓名（需要改）  2021/6/27 0:06
package zap

import (
	"fmt"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/utils/files"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"time"
)



var level zapcore.Level
var prefix string

func InitZap(zapConfig ZapConfig) (logger *zap.Logger) {
	// 判断是否有Director文件夹
	//if ok, _ := files.FileExists(zapConfig.Director); !ok {
	//	fmt.Printf("create %v directory\n", zapConfig.Director)
	//	_ = os.Mkdir(zapConfig.Director, os.ModePerm)
	//}

	files.CreateDirIfNotExists(zapConfig.Director)

	prefix = zapConfig.Prefix

	switch zapConfig.Level { // 初始化配置文件的Level
	case "debug":
		level = zap.DebugLevel
	case "info":
		level = zap.InfoLevel
	case "warn":
		level = zap.WarnLevel
	case "error":
		level = zap.ErrorLevel
	case "dpanic":
		level = zap.DPanicLevel
	case "panic":
		level = zap.PanicLevel
	case "fatal":
		level = zap.FatalLevel
	default:
		level = zap.InfoLevel
	}

	if level == zap.DebugLevel || level == zap.ErrorLevel {
		logger = zap.New(getEncoderCore(zapConfig), zap.AddStacktrace(level))
	} else {
		logger = zap.New(getEncoderCore(zapConfig))
	}
	if zapConfig.ShowLine {
		logger = logger.WithOptions(zap.AddCaller())
	}
	return logger
}

// getEncoderConfig 获取zapcore.EncoderConfig
func getEncoderConfig(zapConfig ZapConfig) (config zapcore.EncoderConfig) {
	config = zapcore.EncoderConfig{
		MessageKey:     "message",
		LevelKey:       "level",
		TimeKey:        "time",
		NameKey:        "logger",
		CallerKey:      "caller",
		StacktraceKey:  zapConfig.StacktraceKey,
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     CustomTimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.FullCallerEncoder,
	}
	switch {
	case zapConfig.EncodeLevel == "LowercaseLevelEncoder": // 小写编码器(默认)
		config.EncodeLevel = zapcore.LowercaseLevelEncoder
	case zapConfig.EncodeLevel == "LowercaseColorLevelEncoder": // 小写编码器带颜色
		config.EncodeLevel = zapcore.LowercaseColorLevelEncoder
	case zapConfig.EncodeLevel == "CapitalLevelEncoder": // 大写编码器
		config.EncodeLevel = zapcore.CapitalLevelEncoder
	case zapConfig.EncodeLevel == "CapitalColorLevelEncoder": // 大写编码器带颜色
		config.EncodeLevel = zapcore.CapitalColorLevelEncoder
	default:
		config.EncodeLevel = zapcore.LowercaseLevelEncoder
	}
	return config
}

// getEncoder 获取zapcore.Encoder
func getEncoder(zapConfig ZapConfig) zapcore.Encoder {
	if zapConfig.Format == "json" {
		return zapcore.NewJSONEncoder(getEncoderConfig(zapConfig))
	}
	return zapcore.NewConsoleEncoder(getEncoderConfig(zapConfig))
}

// getEncoderCore 获取Encoder的zapcore.Core
func getEncoderCore(zapConfig ZapConfig) (core zapcore.Core) {
	writer, err := GetWriteSyncer(zapConfig) // 使用file-rotatelogs进行日志分割
	if err != nil {
		fmt.Printf("Get Write Syncer Failed err:%v", err.Error())
		return
	}
	return zapcore.NewCore(getEncoder(zapConfig), writer, level)
}

// 自定义日志输出时间格式
func CustomTimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.Format(prefix + "2006/01/02 - 15:04:05.000"))
}
