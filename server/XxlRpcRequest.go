package server

import "encoding/json"

const (
	BEAT_ID = "BEAT_PING_PONG"
)

type XxlRpcRequest struct {
	RequestId        string `hessian:"requestId" json:"requestId"`
	CreateMillisTime int64  `hessian:"createMillisTime" json:"createMillisTime"`
	AccessToken      string `hessian:"accessToken" json:"accessToken"`
	ClassName        string `hessian:"className" json:"className"`
	MethodName       string `hessian:"methodName" json:"methodName"`
	//ParameterTypes   []interface{}  `hessian:"parameterTypes" json:"parameterTypes"` // 不知道 java 中的Class对象该对应golang的什么类型，还不如不接收
	Parameters []interface{} `hessian:"parameters" json:"parameters"`
	Version    string        `hessian:"version" json:"version"`
}

func (XxlRpcRequest) JavaClassName() string {
	return "com.xxl.rpc.remoting.net.params.XxlRpcRequest"
}

func (t XxlRpcRequest) String() string {
	data, err := json.Marshal(t)
	if err != nil {
		return ""
	}

	return string(data)
}
