package handler

import (
	"encoding/json"
	. "gitee.com/youbeiwuhuan/go-xxljob-executor/biz/model"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/tools"
)

type JobContext struct {
	JobId int32 `json:"jobId"`

	ExecutorHandler       string `json:"executorHandler"`
	ExecutorParams        string `json:"executorParams"`
	ExecutorBlockStrategy string `json:"executorBlockStrategy"`
	ExecutorTimeout       int    `json:"executorTimeout"`

	LogId       int64 `json:"logId"`
	LogDateTime int64 `json:"logDateTime"`

	//仅仅支持 bean 类型
	GlueType string `json:"glueType"`

	BroadcastIndex int32 `json:"broadcastIndex"`
	BroadcastTotal int32 `json:"broadcastTotal"`
}

func (t JobContext) String() string {
	data, err := json.Marshal(t)
	if err != nil {
		return ""
	}

	return string(data)
}

type IJobHandler interface {
	//初始化方法
	//@author: Frank Fan
	// @param  context JobContext 任务执行上下文
	// @param  xxlLogger *tools.XxlJobFileLogger 向调度器后台上报日志的工具
	Init(context JobContext, xxlLogger *tools.XxlJobFileLogger)
	//销毁方法
	//@author: Frank Fan
	// @param  context JobContext 任务执行上下文
	// @param  xxlLogger *tools.XxlJobFileLogger 向调度器后台上报日志的工具
	Destroy(context JobContext, xxlLogger *tools.XxlJobFileLogger)
	//任务执行逻辑
	//@author: Frank Fan
	//@param param string 后台填写的参数
	// @param  context JobContext 任务执行上下文
	// @param  xxlLogger *tools.XxlJobFileLogger 向调度器后台上报日志的工具
	Execute(param string, context JobContext, xxlLogger *tools.XxlJobFileLogger) ReturnT
}
