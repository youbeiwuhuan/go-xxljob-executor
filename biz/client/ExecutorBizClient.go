package client

import (
	. "gitee.com/youbeiwuhuan/go-xxljob-executor/biz/model"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/utils/https"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/utils/jsons"
	"time"
)

type ExecutorBizClient struct {
	// 接口提供者地址
	addressUrl  string `json:"addressUrl"`
	accessToken string `json:"accessToken"`
	//秒
	timeout int `json:"timeout"`
}

//
// @auth      Frank Fan
// @param     addressUrl string  服务端地址
// @param     accessToken string  权限token
// @param     timeout int  调用超时时间（秒）
// @return
func NewExecutorBizClient(addressUrl string, accessToken string, timeout int) ExecutorBizClient {
	return ExecutorBizClient{
		addressUrl:  addressUrl,
		accessToken: accessToken,
		timeout:     timeout,
	}

}

func (t *ExecutorBizClient) Beat() ReturnT {
	return https.PostJsonXxljob(t.addressUrl+"/beat", "{}", t.accessToken, time.Duration(t.timeout)*time.Second)
}

func (t *ExecutorBizClient) IdleBeat(idleBeatParam IdleBeatParam) ReturnT {
	return https.PostJsonXxljob(t.addressUrl+"/idleBeat", jsons.ToJsonString(idleBeatParam), t.accessToken, time.Duration(t.timeout)*time.Second)
}

func (t *ExecutorBizClient) Run(triggerParam TriggerParam) ReturnT {
	return https.PostJsonXxljob(t.addressUrl+"/run", jsons.ToJsonString(triggerParam), t.accessToken, time.Duration(t.timeout)*time.Second)
}

func (t *ExecutorBizClient) Kill(killParam KillParam) ReturnT {
	return https.PostJsonXxljob(t.addressUrl+"/kill", jsons.ToJsonString(killParam), t.accessToken, time.Duration(t.timeout)*time.Second)
}

func (t *ExecutorBizClient) Log(logParam LogParam) ReturnT {
	return https.PostJsonXxljob(t.addressUrl+"/log", jsons.ToJsonString(logParam), t.accessToken, time.Duration(t.timeout)*time.Second)
}
