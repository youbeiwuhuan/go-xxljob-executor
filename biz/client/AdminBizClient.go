package client

import (
	. "gitee.com/youbeiwuhuan/go-xxljob-executor/biz/model"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/utils/https"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/utils/jsons"
	"time"
)

type AdminBizClient struct {
	// 接口提供者地址
	addressUrl  string `json:"addressUrl"`
	accessToken string `json:"accessToken"`
	//秒
	timeout int `json:"timeout"`
}

//
// @auth      Frank Fan
// @param     addressUrl string  服务端地址
// @param     accessToken string  权限token
// @param     timeout int  调用超时时间（秒）
// @return
func NewAdminBizClient(addressUrl string, accessToken string, timeout int) *AdminBizClient {
	return &AdminBizClient{
		addressUrl:  addressUrl,
		accessToken: accessToken,
		timeout:     timeout,
	}

}

func (t *AdminBizClient) Callback(callbackParamList []HandleCallbackParam) ReturnT {
	return https.PostJsonXxljob(t.addressUrl+"/api/callback", jsons.ToJsonString(callbackParamList), t.accessToken, time.Duration(t.timeout)*time.Second)
}

func (t *AdminBizClient) Registry(registryParam RegistryParam) ReturnT {
	return https.PostJsonXxljob(t.addressUrl+"/api/registry", jsons.ToJsonString(registryParam), t.accessToken, time.Duration(t.timeout)*time.Second)
}

func (t *AdminBizClient) RegistryRemove(registryParam RegistryParam) ReturnT {
	return https.PostJsonXxljob(t.addressUrl+"/api/registryRemove", jsons.ToJsonString(registryParam), t.accessToken, time.Duration(t.timeout)*time.Second)
}
