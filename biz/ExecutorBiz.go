package biz

import . "gitee.com/youbeiwuhuan/go-xxljob-executor/biz/model"

type ExecutorBiz interface {
	//  回应调度期心跳询问
	// @auth      Frank Fan
	// @return
	Beat() ReturnT
	//  上报心跳
	// @auth      Frank Fan
	// @param     idleBeatParam IdleBeatParam 心跳信息
	// @return
	IdleBeat(idleBeatParam IdleBeatParam) ReturnT
	//  执行任务
	// @auth      Frank Fan
	// @param     triggerParam TriggerParam 触发任务信息
	// @return
	Run(triggerParam TriggerParam) ReturnT
	//  杀死任务
	// @auth      Frank Fan
	// @param     killParam KillParam 参数
	// @return
	Kill(killParam KillParam) ReturnT

	//  查询任务日志
	// @auth      Frank Fan
	// @param     logParam LogParam 查询参数
	// @return
	Log(logParam LogParam) ReturnT
}
