package model

import "encoding/json"

type LogParam struct {
	LogDateTim  int64 `json:"logDateTim"`
	LogId       int64 `json:"logId"`
	FromLineNum int32 `json:"fromLineNum"`
}

func (t LogParam) String() string {
	data, err := json.Marshal(t)
	if err != nil {
		return ""
	}

	return string(data)
}
