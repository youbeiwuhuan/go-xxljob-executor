package model

import (
	"encoding/json"
)

const (
	HANDLE_COCE_SUCCESS = 200
	HANDLE_COCE_FAIL    = 500
	HANDLE_COCE_TIMEOUT = 502
)

// 执行器回传给调度器数据，主要是运行结果
type HandleCallbackParam struct {
	LogId      int64 `json:"logId"`
	LogDateTim int64 `json:"logDateTim"`

	HandleCode int32  `json:"handleCode"`
	HandleMsg  string `json:"handleMsg"`
}

func (t HandleCallbackParam) String() string {
	data, err := json.Marshal(t)
	if err != nil {
		return ""
	}

	return string(data)
}
