package model

import "encoding/json"

type LogResult struct {
	FromLineNum int32  `json:"fromLineNum"`
	ToLineNum   int32  `json:"toLineNum"`
	LogContent  string `json:"logContent"`
	IsEnd       bool   `json:"isEnd"`
}

func (t LogResult) String() string {
	data, err := json.Marshal(t)
	if err != nil {
		return ""
	}

	return string(data)
}
