package model

import (
	"encoding/json"
)

type IdleBeatParam struct {
	JobId int32   `json:"jobId"`
}



func (t IdleBeatParam) String() string {
	data, err := json.Marshal(t)
	if err != nil {
		return ""
	}

	return string(data)
}