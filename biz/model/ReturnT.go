package model

import (
	"encoding/json"
)

const (
	SUCCESS_CODE = 200
	FAIL_CODE    = 500
	//UNKONWN      = 404
)

type ReturnT struct {
	Code    int32       `json:"code"`
	Msg     string      `json:"msg"`
	Content interface{} `json:"content"`
}

func (t ReturnT) String() string {
	data, err := json.Marshal(t)
	if err != nil {
		return ""
	}

	return string(data)
}

func NewReturnT(code int32, msg string, content interface{}) ReturnT {
	return ReturnT{
		Code:    code,
		Msg:     msg,
		Content: content,
	}
}
