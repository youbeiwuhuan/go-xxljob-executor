package biz

import . "gitee.com/youbeiwuhuan/go-xxljob-executor/biz/model"

type AdminBiz interface {

	//  上报任务运行结果
	// @auth      Frank Fan
	// @param     callbackParamList []HandleCallbackParam 任务运行结果
	// @return
	Callback(callbackParamList []HandleCallbackParam) ReturnT

	// 注册执行器
	// @auth      Frank Fan
	// @param     registryParam RegistryParam 任务运行结果
	// @return
	Registry(registryParam RegistryParam) ReturnT

	// 移除执行器注册信息
	// @auth      Frank Fan
	// @param     registryParam RegistryParam 任务运行结果
	// @return
	RegistryRemove(registryParam RegistryParam) ReturnT
}
