package main

import (
	"fmt"
	. "gitee.com/youbeiwuhuan/go-xxljob-executor"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/main/handler"
	"time"
)

func main() {
	//
	//e := XxljobExcutor{
	//	adminAddresses: "http://127.0.0.1:8080/xxl-job-admin",
	//	accessToken:    "",
	//	appname:        "gitee.com/youbeiwuhuan/go-xxljob-executor",
	//
	//	address:          "",
	//	ip:               "127.0.0.1",
	//	port:             22,
	//	logPath:          "D:/tmp/gitee.com/youbeiwuhuan/go-xxljob-executor",
	//	logRetentionDays: 60,
	//}

	e := NewXxljobExcutor("http://127.0.0.1:8080/xxl-job-admin",
		"",
		"gitee.com/youbeiwuhuan/go-xxljob-executor",

		"http://127.0.0.1:9222",
		"127.0.0.1",
		9222,
		"D:/tmp/gitee.com/youbeiwuhuan/go-xxljob-executor",
		60)

	e.AddJobHandler("HelloJobHandler", &handler.HelloJobHandler{})

	e.Start()

	for {
		time.Sleep(time.Duration(30) * time.Second)
		fmt.Println("----------example run----------------------------")
	}

}
