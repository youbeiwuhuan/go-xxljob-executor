package handler

import (
	. "gitee.com/youbeiwuhuan/go-xxljob-executor/biz/model"
	. "gitee.com/youbeiwuhuan/go-xxljob-executor/global"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/handler"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/tools"
	"time"
)

type HelloJobHandler struct {
}

func (t *HelloJobHandler) Init(context handler.JobContext, xxlLogger *tools.XxlJobFileLogger) {

	Logger.Info("----------HelloJobHandler----Init------------------------" + context.String())

}
func (t *HelloJobHandler) Destroy(context handler.JobContext, xxlLogger *tools.XxlJobFileLogger) {
	Logger.Info("----------HelloJobHandler----Destroy------------------------" + context.String())
}
func (t *HelloJobHandler) Execute(param string, context handler.JobContext, xxlLogger *tools.XxlJobFileLogger) ReturnT {
	Logger.Info("----------HelloJobHandler----Execute------------------------" + context.String())
	time.Sleep(5 * time.Second)
	xxlLogger.Log("----------HelloJobHandler----Execute------------------------" + context.String())

	return ReturnT{Code: SUCCESS_CODE}
}
