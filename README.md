# gitee.com/youbeiwuhuan/go-xxljob-executor







#### 介绍
go实现的xxljob执行器,仅支持BEAN模式执行器,不支持GlueType模式。


#### 分支说明
dev-XXX 分支 对应xxl-job-XXX 版本的调度器。当前正在开发 dev2.1.2 分支，对应官方tag 2.1.2.
xxl-job 2.0以下版本太老就不适配了

| 分支        | 描述    |  状态  |
| --------   | :-----   | :---- |
| master        | 主分支      |   代码和dev2.3.0 相同   |
| dev2.3.0        | 对应Java版执行器2.3.0      |   开发完成，与xxl-job-2.3.0后台联调测试通过    |
| dev2.1.2        | 对应Java版执行器2.1.2      |   开发完成，与xxl-job-2.1.2后台联调测试通过 (2.1.1 联调也通过)    |





##### dev2.3.0 分支(仅引入了web包gin和日志包zap,其他都是go原生包)
除了不支持GlueType模式任务外（个人觉得go的执行器没必要实现GlueType模式任务，要用GlueType模式任务直接用java版执行器更合适），
基本实现了所有的执行器功能。几乎是将对着java执行器代码用go翻译了一遍。因为本人现在的饭碗还是java,对go还是不熟练，代码肯定有WTF,请谅解！


#### 使用说明
示例说明
进入main 目录下，修改example.go，运行即可


![image](https://gitee.com/youbeiwuhuan/go-xxljob-executor/raw/master/go-job-config.png)



##### 导包
go get https://gitee.com/youbeiwuhuan/go-xxljob-executor

##### 代码示例

```go
package hanlder

import (
	. "gitee.com/youbeiwuhuan/go-xxljob-executor/biz/model"
	. "gitee.com/youbeiwuhuan/go-xxljob-executor/global"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/handler"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/tools"
	"time"
)

type HelloJobHandler struct {
}

func (t *HelloJobHandler) Init(context handler.JobContext, xxlLogger *tools.XxlJobFileLogger) {

	Logger.Info("----------HelloJobHandler----Init------------------------" + context.String())

}
func (t *HelloJobHandler) Destroy(context handler.JobContext, xxlLogger *tools.XxlJobFileLogger) {
	Logger.Info("----------HelloJobHandler----Destroy------------------------" + context.String())
}
func (t *HelloJobHandler) Execute(param string, context handler.JobContext, xxlLogger *tools.XxlJobFileLogger) ReturnT {
	Logger.Info("----------HelloJobHandler----Execute------------------------" + context.String())
	time.Sleep(5 * time.Second)
	xxlLogger.Log("----------HelloJobHandler----Execute------------------------" + context.String())

	return ReturnT{Code: SUCCESS_CODE}
}

```

```go
package main

import (
	"fmt"
	. "gitee.com/youbeiwuhuan/go-xxljob-executor"
	"gitee.com/youbeiwuhuan/go-xxljob-executor/main/handler"
	"time"
)

func main() {
	e := NewXxljobExcutor("http://127.0.0.1:8080/xxl-job-admin",
		"",
		"gitee.com/youbeiwuhuan/go-xxljob-executor",

		"http://127.0.0.1:9222",
		"127.0.0.1",
		9222,
		"D:/tmp/gitee.com/youbeiwuhuan/go-xxljob-executor",
		60)

	e.AddJobHandler("HelloJobHandler", &handler.HelloJobHandler{})

	e.Start()

	for {
		time.Sleep(time.Duration(30) * time.Second)
		fmt.Println("----------example run----------------------------")
	}

}
```

